window.addEventListener("DOMContentLoaded", function(e) {
	var swiper = new Swiper('.swiper-container', {
      spaceBetween: 30,
	  speed : 1500,
	  loop: true,
	  
      effect: 'fade',
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
      },
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
    });
}, false);